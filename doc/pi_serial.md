## Enable and configure UART on raspberry >= Pi3
Enter following command in Terminal to enable UART
```bash
sudo raspi-config
```
Select -> **Interfacing Options**  

![schema1](pics/serial_conf1.png "schema_1")  
After selecting Interfacing option, select **Serial** option to enable UART  

![schema2](pics/serial_conf2.png "schema_2")  
Then it will ask for login shell to be accessible over Serial, select **No** shown as follows. 

![schema3](pics/serial_conf3.png "schema_3")  

At the end, it will ask for enabling Hardware Serial port, select **Yes**  

![schema4](pics/serial_conf4.png "schema_4")  

![schema5](pics/serial_conf5.png "schema_5")

Remap the PL011 uart by editing the file /boot/config.txt:
```bash
nano /boot/config.txt
```
Add the following line at the end of file:
```bash
dtoverlay=pi3-miniuart-bt
```
Edit the file cmdline.txt
```bash
sudo nano /boot/cmdline.txt
```
delete following words if they are present
```code
console=serial0,115200
```

Finally, reboot the raspberry:
```bash
sudo reboot
```


## Wiring gps module
Connect the Ground, +5V, raspberry Rx to GPS Tx and optionally Raspberry Tx to GPS Rx following the schema.  
![schema4](pics/serial_gpio.jpg "schema_4") 