package cmd

// dynamic go build vars (ldflags -X)
var (
	Version   = "dev"
	BuildDate = "unknown"
)

const (
	Repo   = "gitlab.com/lemoidului/ogps"
	Banner = `
  ██████╗  ██████╗ ██████╗ ███████╗
 ██╔═══██╗██╔════╝ ██╔══██╗██╔════╝
 ██║   ██║██║  ███╗██████╔╝███████╗
 ██║   ██║██║   ██║██╔═══╝ ╚════██║
 ╚██████╔╝╚██████╔╝██║     ███████║
  ╚═════╝  ╚═════╝ ╚═╝     ╚══════╝                                 
`
)
