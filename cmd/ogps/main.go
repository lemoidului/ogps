package main

import (
	"context"
	"flag"
	"fmt"
	"ogps/cmd"
	"ogps/gps"
	"ogps/ogn"
	"os"
	"runtime"

	"github.com/coreos/go-systemd/daemon"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/journald"
	"github.com/rs/zerolog/log"
)

func main() {
	fmt.Println(cmd.Banner)
	fmt.Printf("Repo        : %s\n", cmd.Repo)
	fmt.Printf("Version     : %s\n", cmd.Version)
	fmt.Printf("Build       : %s\n", cmd.BuildDate)
	fmt.Printf("Go Ver      : %s\n\n", runtime.Version())

	verbose := flag.Bool("v", false, "verbose mode")
	vverbose := flag.Bool("vv", false, "very verbose mode")
	test := flag.Bool("t", false, "testing mode")
	sname := flag.String("s", "/dev/serial0", "gps serial name")
	sbrate := flag.Uint("b", 9600, "gps serial baud rate")
	flag.Parse()

	// OGPSYSD env is set in ogps systemd unit
	if _, ogpsysd := os.LookupEnv("OGPSYSD"); ogpsysd {
		log.Logger = log.Output(journald.NewJournalDWriter())
	} else {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr,
			TimeFormat: "15:04:05"})
	}
	if *vverbose {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		gps.SetVerbose()
		log.Debug().Msg("running in very verbose mode")
	} else if *verbose {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
		log.Debug().Msg("running in verbose mode")
	} else {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	if *test {
		log.Info().Msg("running in testing mode")
	}
	log.Info().Msgf("Trying to open serial port %s with %d baudrate",
		*sname, *sbrate)
	err := gps.OpenSerial(*sname, *sbrate)
	if err != nil {
		log.Fatal().Err(err).Msg("open serial fail")
	}
	defer gps.CloseSerial()
	// systemd daemon notify
	_, err = daemon.SdNotify(false, daemon.SdNotifyReady)
	if err != nil {
		log.Err(err).Msg("systemd notify error")
	}

	resp := make(chan *gps.Fix)
	ctx, cancel := context.WithCancel(context.Background())
	go gps.GetFix(cancel, resp)
	run := true
	for run {
		select {
		case fix := <-resp:
			log.Info().Float32("alt", float32(fix.Alt)).
				Float32("lat", float32(fix.Lat)).
				Float32("lng", float32(fix.Lng)).
				Float32("hdop", float32(fix.Hdop)).
				Float32("geoid", float32(fix.Geoid)).
				Msg("Switching fix")
			err = ogn.WriteRestart(fix, *test)
			if err != nil && !(*test) {
				log.Fatal().Err(err).Msg("Program End")
			}

		case <-ctx.Done():
			log.Warn().Msg("Program end")
			run = false
		}
	}
}
