package gps

import (
	"bufio"
	"context"
	"io"
	"math"
	"time"

	"github.com/adrianmo/go-nmea"
	"github.com/jacobsa/go-serial/serial"

	"github.com/rs/zerolog/log"
)

var (
	serialPort io.ReadWriteCloser
	verbose    bool = false
)

const (
	VALID_DELAY = 30 // max time frame in s for fix average calculation
	NB_FIX      = 6  // nb of fixes for average calculation
	DISTABS     = 30 // absolute distance in m to switch position
	DISTHDOP    = 20 // distance in m to switch position if hdop is better
	ALTHDOP     = 25 // altitude difference in m to switch if hdop is better
)

type Fix struct {
	valid bool
	Lat   float64 // latitude in decimal degrees
	Lng   float64 // longitude in decimal degrees
	Alt   float64 // altitude in meters
	Hdop  float64 // horizontal dilution of precision, the lowest the better
	Geoid float64 // geoid separation in m (gps module produce simplified model)
	time  time.Time
}

func OpenSerial(name string, br uint) (err error) {
	options := serial.OpenOptions{
		PortName:        name,
		BaudRate:        br,
		DataBits:        8, // common options for most UART GPS
		StopBits:        1, //
		MinimumReadSize: 4, //
	}
	serialPort, err = serial.Open(options)
	return err
}

func CloseSerial() error {
	return serialPort.Close()
}

func SetVerbose() {
	verbose = true
}

// get position and push the result in resp channel
// cancel function is used to cancel context when serial port is disconnected
func GetFix(cancel context.CancelFunc, resp chan *Fix) {
	reader := bufio.NewReader(serialPort)
	scanner := bufio.NewScanner(reader)
	feed := make(chan *Fix)
	go fixAvg(feed, resp)
	for scanner.Scan() {
		if verbose {
			log.Debug().Msg(scanner.Text())
		}
		f, err := parseNmea(scanner.Text())
		if err != nil {
			log.Err(err).Msg("nmea parsing")
			continue
		}
		feed <- f
	}
	log.Error().Msg("Serial disconnect, stoping program")
	cancel()
}

// parse Nmea sentence and return position based on GGA type
// return error or or nil if sentence it's not a GGA type
func parseNmea(sentence string) (*Fix, error) {
	s, err := nmea.Parse(sentence)
	if err != nil {
		return nil, err
	}
	f := &Fix{}
	if s.DataType() == nmea.TypeGGA {
		m := s.(nmea.GGA)
		if m.FixQuality == "1" || m.FixQuality == "2" {
			f.valid = true
			f.Lat = m.Latitude
			f.Lng = m.Longitude
			f.Alt = math.Abs(m.Altitude) // OGN process not accept neg alt
			f.Geoid = m.Separation
			f.Hdop = m.HDOP
			f.time = time.Now()
			return f, nil
		} else {
			f.valid = false
			return f, nil
		}
	}
	return nil, nil
}

// calculate moving average with NB_FIX for a maxium VALID_DELAY
// position /altitude are refine thanks to horizontal dilution of precision.
func fixAvg(feed chan *Fix, resp chan *Fix) {
	var fixes []*Fix
	favg := &Fix{valid: true}
	fold := Fix{}
	log_wait := true
	cfix := make(chan Fix)
	go fixDelay(cfix, resp)
	for {
		f := <-feed
		if f == nil {
			continue
		}
		if !f.valid {
			if log_wait {
				log.Info().Msg("Awaiting valid fixes")
			}
			log_wait = false
			continue
		}
		log_wait = true
		fixes = append(fixes, f)
		if len(fixes) < NB_FIX {
			continue
		}
		if fixes[NB_FIX-1].time.Sub(fixes[0].time).Seconds() > VALID_DELAY {
			fixes = fixes[1:]
			continue
		}
		var lat_sum, lng_sum, alt_sum, hdop_sum float64
		/* Calculating an average position between several fixes normally
		requires a formula such as circular mean (https://en.wikipedia.org/wiki/Circular_mean)
		However, this adds a great deal of complexity and should not be
		necessary for usual positions used by OGN receivers. */
		for _, elt := range fixes {
			lat_sum += elt.Lat
			lng_sum += elt.Lng
			alt_sum += elt.Alt
			hdop_sum += elt.Hdop
		}
		favg.time = fixes[NB_FIX-1].time
		favg.Lat = lat_sum / NB_FIX
		favg.Lng = lng_sum / NB_FIX
		favg.Alt = alt_sum / NB_FIX
		favg.Geoid = fixes[NB_FIX-1].Geoid
		favg.Hdop = hdop_sum / NB_FIX
		fixes = fixes[1:]
		if fold == (Fix{}) {
			fold = *favg
			log.Debug().Float32("lat", float32(favg.Lat)).
				Float32("lng", float32(favg.Lng)).
				Float32("alt", float32(favg.Alt)).
				Float32("geoid", float32(favg.Geoid)).
				Float32("hdop", float32(favg.Hdop)).Msg("get fix")
			cfix <- *favg
		} else {
			dist := distance(favg, &fold)
			altd := math.Abs(favg.Alt - fold.Alt)
			log.Debug().Float32("lat", float32(favg.Lat)).
				Float32("lng", float32(favg.Lng)).
				Float32("alt", float32(favg.Alt)).
				Float32("geoid", float32(favg.Geoid)).
				Float32("hdop", float32(favg.Hdop)).Msg("get fix")
			log.Debug().Float32("distance", float32(dist)).
				Float32("alt_diff", float32(altd)).Msg("")
			if DISTABS < dist {
				log.Info().Msgf("fix will move %f m away", dist)
				fold = *favg
				cfix <- *favg
			} else if (DISTHDOP < dist) && (favg.Hdop <= fold.Hdop) {
				log.Info().Msgf("position will be refine shortly %f m away ", dist)
				fold = *favg
				cfix <- *favg
			} else if (ALTHDOP < altd) && (favg.Hdop <= fold.Hdop) {
				log.Info().Msgf("altitude will be refine shortly to %f", favg.Alt)
				fold = *favg
				cfix <- *favg
			}
		}
	}
}

// distance calculate short path in meters between two fix
// based on haversine formula
func distance(f1 *Fix, f2 *Fix) float64 {

	lat1 := f1.Lat * math.Pi / 180
	lng1 := f1.Lng * math.Pi / 180
	lat2 := f2.Lat * math.Pi / 180
	lng2 := f2.Lng * math.Pi / 180

	diffLat := lat2 - lat1
	diffLng := lng2 - lng1
	a := math.Pow(math.Sin(diffLat/2), 2) + math.Cos(lat1)*math.Cos(lat2)*
		math.Pow(math.Sin(diffLng/2), 2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	return c * 6371000
}

// to avoid too many changes on a short time, fixDelay
// will return the last given fix in resp channel after a delay.
// The first response is done immediatly, others response with 30 secondes delay
func fixDelay(cfix chan Fix, resp chan *Fix) {
	var f Fix
	ticker := time.NewTicker(30 * time.Second)
	firstfix, newfix := false, false
	defer ticker.Stop()
	for {
		select {
		case f = <-cfix:
			{
				if !firstfix {
					resp <- &f
					ticker.Reset(30 * time.Second)
					firstfix = true
				} else {
					newfix = true
				}
			}
		case <-ticker.C:
			{
				if newfix {
					resp <- &f
					newfix = false
				}
			}
		}
	}
}
