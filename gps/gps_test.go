package gps

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func Test_func(t *testing.T) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	err := OpenSerial("/dev/ttyUSB0", 9600)
	if err != nil {
		t.Fatal(err.Error())
	}
	defer CloseSerial()
	resp := make(chan *Fix)
	ctx, cancel := context.WithCancel(context.Background())
	go GetFix(cancel, resp)
	run := true
	for run {
		select {
		case fix := <-resp:
			fmt.Printf("switch fix: %f, %f, %f, %f\n", fix.Lat, fix.Lng,
				fix.Alt, fix.Hdop)
		case <-ctx.Done():
			fmt.Println("end test")
			run = false
		}
	}
}
