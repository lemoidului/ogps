#!/bin/bash
set -e
[[ $(whoami) =~ "root" ]] || { echo "error, script must be run as root" ; exit 1; }
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH
echo "1/4: copying binaries files"
cp ogps-arm /usr/local/sbin/ogps
echo "2/4: installing ogps systemd unit"
cp ogps.service /etc/systemd/system/
systemctl daemon-reload
echo "3/4: enable ogps systemd unit"
systemctl enable ogps.service
echo "4/4: ogps installed, start ogps daemon"

