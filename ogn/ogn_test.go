package ogn

import (
	"fmt"
	"os"
	"testing"
)

var data1 = `
RF:
{ 
  FreqCorr = +0;          # [ppm]      "big" R820T sticks have 40-80ppm correction factors, measure it with gsm_scan
  BiasTee = 1;
  GSM:                       # for frequency calibration based on GSM signals
  { CenterFreq  = 926.800;   # [MHz] find the best GSM frequency with gsm_scan
    Gain        = 25.0;      # [dB]  RF input gain (beware that GSM signals are very strong !)
  } ;
  OGN:
  { Gain        =  49;
  } ;
} ;

Position:
{ Latitude   =  +44.245619; # test
  # Longitude  =  4.011476 ;
  # Longitude  =  4.011476;
  Longitude  =          5.011476 ;
  Altitude   =   495;   # Altitude above sea leavel (m)
} ;

APRS:
{ Call = "LEMOI";     # APRS callsign (9 char max)
} ;
`

var data2 = `
RF:
{ 
  FreqCorr = +0;          # [ppm]      "big" R820T sticks have 40-80ppm correction factors, measure it with gsm_scan
  BiasTee = 1;
  GSM:                       # for frequency calibration based on GSM signals
  { CenterFreq  = 926.800;   # [MHz] find the best GSM frequency with gsm_scan
    Gain        = 25.0;      # [dB]  RF input gain (beware that GSM signals are very strong !)
  } ;
  OGN:
  { Gain        =  49;
  } ;
} ;

Position:
{ # Latitude   =  +44.245619; # test
  # Longitude  =  4.011476 ;
  # Longitude  =  4.011476;
  Longitude  =          5.011476 ;
  Altitude   =   495;   # Altitude above sea leavel (m)
} ;

APRS:
{ Call = "LEMOI";     # APRS callsign (9 char max)
} ;
`
var data3 = `
RF:
{ 
  FreqCorr = +0;          # [ppm]      "big" R820T sticks have 40-80ppm correction factors, measure it with gsm_scan
  BiasTee = 1;
  GSM:                       # for frequency calibration based on GSM signals
  { CenterFreq  = 926.800;   # [MHz] find the best GSM frequency with gsm_scan
    Gain        = 25.0;      # [dB]  RF input gain (beware that GSM signals are very strong !)
  } ;
  OGN:
  { Gain        =  49;
  } ;
} ;

Position:
	{
	 Latitude   =  +44.245619; # test
  # Longitude  =  4.011476 ;
  # Longitude  =  4.011476;
  Longitude  =          5.011476 ;
  Altitude   =   495;   # Altitude above sea leavel (m)
} ;

APRS:
{ Call = "LEMOI";     # APRS callsign (9 char max)
} ;
`

var data4 = `
### Mandatory ###
ReceiverName="Ogps" # Please follow naming convention (http://wiki.glidernet.org/receiver-naming-convention)
#Latitude="0"
Latitude="46.329933"
#Latitude="0"
Longitude="6.641704"
Altitude="813"

### Optional ###
piUserPassword="ogps" # By default pi user login by password is disabled
# runAtBoot="" # Allow a specific script to be run at boot time
# FreqCorr=""
# GSMCenterFreq=""
# GSMGain=""
# wifiName=""
# wifiPassword=""
# wifiCountry=""
`

func Test_parseFactory1(t *testing.T) {
	lat, err := parseFactory(data1, "Latitude")
	if err != nil {
		fmt.Println(err.Error())
		t.Fatal(err)
	}
	fmt.Println("Latitude:", lat)
	lng, err := parseFactory(data1, "Longitude")
	if err != nil {
		fmt.Println(err.Error())
		t.Fatal(err)
	}
	fmt.Println("Longitude:", lng)
	alt, err := parseFactory(data1, "Altitude")
	if err != nil {
		fmt.Println(err.Error())
		t.Fatal(err)
	}
	fmt.Println("Altitude:", alt)

}

func Test_parseFactory2(t *testing.T) {
	_, err := parseFactory(data2, "Latitude")
	if err == nil {
		t.Fatal()
	}
	fmt.Println(err.Error())
}

func Test_parseFactory3(t *testing.T) {
	_, err := parseFactory(data3, "Latitude")
	if err != nil {
		t.Fatal(err.Error())
	}
}

func Test_parseFactory4(t *testing.T) {
	_, err := parseFactory(data2, "toto")
	if err == nil {
		t.Fatal()
	}
	fmt.Println(err.Error())
}

func Test_parseFactory5(t *testing.T) {
	lat, err := parseFactory(data4, "Latitude")
	if err != nil {
		fmt.Println(err.Error())
		t.Fatal()
	}
	if lat != "\"46.329933\"" {
		t.Fatal()
	}
}

func Test_WriteConf(t *testing.T) {
	data, err := os.ReadFile("./rtlsdr-ogn.conf")
	err = os.WriteFile("./rtlsdr-ogn.test", data, 0644)
	if err != nil {
		t.Fatal(err.Error())
	}
	pos := &position{lat: "45.245619", lng: "5.011476", alt: "595"}

	err = writeConf("./rtlsdr-ogn.test", pos)
	if err != nil {
		t.Fatal(err.Error())
	}
}

func Test_FindProcess(t *testing.T) {
	p, err := findProcess("vim")
	fmt.Println(p, err)
}
