package ogn

import (
	"errors"
	"fmt"
	"ogps/gps"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"github.com/rs/zerolog/log"

	"github.com/shirou/gopsutil/v3/process"
)

const (
	p1_name = "ogn-rf"
	p2_name = "ogn-decode"
	fat_cnf = "/boot/OGN-receiver.conf"
)

type position struct {
	lat string
	lng string
	alt string
}

func WriteRestart(fix *gps.Fix, test bool) (err error) {
	var p1, p2 *process.Process
	p1, err = findProcess(p1_name)
	if err != nil {
		log.Err(err).Msg("findProcess error")
		return err
	}
	if p1 == nil {
		return errors.New(fmt.Sprintf("unable to find ogn process %s", p1_name))
	}
	p2, err = findProcess(p2_name)
	if err != nil {
		log.Err(err).Msg("findProcess error")
		return err
	}
	if p2 == nil {
		return errors.New(fmt.Sprintf("unable to find ogn process %s", p2_name))
	}
	confPath, err := findConf(p1)
	if err != nil {
		log.Err(err).Msg("findConf error")
		return errors.New("unable to find ogn configuration file")
	}
	log.Info().Msgf("find ogn configuration file: %s", confPath)
	if !test {
		// write conf currently use by process
		err = writeConf(confPath, fix2pos(fix))
		if err != nil {
			log.Err(err).Msg("writeConf error")
			return errors.New("unable to write ogn configuration file")
		}
		// write config file in Fat partition if present (OGN image)
		if stat, err := os.Stat(fat_cnf); err == nil {
			if !stat.IsDir() {
				err = writeConfFat(fat_cnf, fix2pos(fix))
				if err != nil {
					log.Warn().Err(err).Msgf("unable to write configuration on fat")
				}
			}
		}

	} else {
		log.Info().Msg("faking write into configuration")
	}
	if !test {
		// ogn daemon will restart automaticaly the process
		log.Info().Msg("configuration file written, ogn process will be restart")
		err = p1.Kill()
		err = p2.Kill()
		if err != nil {
			log.Err(err).Msgf("error while killing ogn process")
			return errors.New("error, can not stop ogn processs, check your rights")
		}
	} else {
		log.Info().Msg("faking restart ogn process")
	}
	return nil
}

// convert a gps Fix structure into ogn position struct
func fix2pos(fix *gps.Fix) *position {
	return &position{lat: fmt.Sprintf("%v", float32(fix.Lat)),
		lng: fmt.Sprintf("%v", float32(fix.Lng)),
		alt: fmt.Sprintf("%v", int(fix.Alt))}
}

// search and return the first process with a given name
// try 3 times in a max delay of 15 secondes if not found
func findProcess(pName string) (*process.Process, error) {
	nbtry := 1
	for {
		processes, err := process.Processes()
		if err != nil {
			return nil, err
		}
		for _, p := range processes {
			name, err := p.Name()
			if err != nil {
				return nil, err
			}
			if strings.Contains(name, pName) {
				return p, nil
			}
		}
		nbtry += 1
		if nbtry > 3 {
			break
		}
		log.Warn().Msgf("unable to find process %s, retry in 5 s", pName)
		time.Sleep(5 * time.Second)
	}
	log.Warn().Msgf("unable to find process %s", pName)
	return nil, nil
}

// return the first cmdline arg path for a given Process
func findConf(p *process.Process) (string, error) {
	cmd, err := p.CmdlineSlice()
	cwd, err := p.Cwd()
	if err != nil {
		return "", err
	}
	if filepath.IsAbs(cmd[1]) {
		return cmd[1], nil
	} else {
		return path.Join(cwd, cmd[1]), nil
	}
}

func parseFactory(data string, key string) (string, error) {
	reg_string := fmt.Sprintf(
		`[\t ]*{?\s*#?[\t ]*%s[\t ]*=[\t ]*("?[+-]?[\t ]*[0-9]+[.]?[[0-9]*]?"?[\t ]*;?)`,
		key)
	reg := regexp.MustCompile(reg_string)
	matches := reg.FindAllStringSubmatch(data, -1)
	if matches == nil {
		return "",
			errors.New(fmt.Sprintf("%s not found in ogn configuration", key))
	}
	reg_string = fmt.Sprintf(`[\t ]*#[\t ]*%s`, key)
	reg = regexp.MustCompile(reg_string)
	val := ""
	for _, match := range matches {
		if len(match) != 2 {
			continue
		}
		if reg.MatchString(match[0]) {
			continue
		}
		val = match[1]
	}
	if val == "" {
		return "",
			errors.New(fmt.Sprintf("%s not found in ogn configuration", key))
	}
	return val, nil
}

func writeConf(path string, pos *position) error {
	datab, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	data := string(datab)
	lat, err := parseFactory(data, "Latitude")
	lng, err := parseFactory(data, "Longitude")
	alt, err := parseFactory(data, "Altitude")
	if err != nil {
		return err
	}
	r := strings.NewReplacer(lat, fmt.Sprintf("%s;", pos.lat),
		lng, fmt.Sprintf("%s;", pos.lng),
		alt, fmt.Sprintf("%s;", pos.alt))
	data = r.Replace(data)
	err = os.WriteFile(path, []byte(data), 0644)
	return err
}

func writeConfFat(path string, pos *position) error {
	datab, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	data := string(datab)
	lat, err := parseFactory(data, "Latitude")
	lng, err := parseFactory(data, "Longitude")
	alt, err := parseFactory(data, "Altitude")
	if err != nil {
		return err
	}
	r := strings.NewReplacer(lat, fmt.Sprintf(`"%s"`, pos.lat),
		lng, fmt.Sprintf(`"%s"`, pos.lng),
		alt, fmt.Sprintf(`"%s"`, pos.alt))
	data = r.Replace(data)
	err = os.WriteFile(path, []byte(data), 0644)
	return err
}
