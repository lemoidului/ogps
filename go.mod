module ogps

go 1.17

require (
	github.com/adrianmo/go-nmea v1.8.0
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4
	github.com/rs/zerolog v1.30.0
	github.com/shirou/gopsutil/v3 v3.23.7
)

require (
	github.com/coreos/go-systemd/v22 v22.5.0 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/lufia/plan9stats v0.0.0-20211012122336-39d0f177ccd0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/power-devops/perfstat v0.0.0-20210106213030-5aafc221ea8c // indirect
	github.com/shoenig/go-m1cpu v0.1.6 // indirect
	github.com/tklauser/go-sysconf v0.3.11 // indirect
	github.com/tklauser/numcpus v0.6.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.3 // indirect
	golang.org/x/sys v0.11.0 // indirect
)
