![schema5](doc/pics/logo2.png?raw=true "logo2")  
# Ogn receiver GPS daemon
## Description
OGPS is a GPS location service designed to run on [ogn receivers](http://wiki.glidernet.org), automatically configuring position of receiver for the rtlsdr-ogn service without manual intervention by the user.  
OGPS is useful for mobile autonomus ogn stations.  
OGPS need a [OGN receiver](http://wiki.glidernet.org/ogn-receiver-installation) and an additional UART TTL gps module.   
OGPS is written in Go and released under the GNU GPL v3 license.
## Usage
```bash
./ogps --help

  ██████╗  ██████╗ ██████╗ ███████╗
 ██╔═══██╗██╔════╝ ██╔══██╗██╔════╝
 ██║   ██║██║  ███╗██████╔╝███████╗
 ██║   ██║██║   ██║██╔═══╝ ╚════██║
 ╚██████╔╝╚██████╔╝██║     ███████║
  ╚═════╝  ╚═════╝ ╚═╝     ╚══════╝                                 

Repo        : gitlab.com/lemoidului/ogps
Version     : v0.1-1-gcb9a5c1
Build       : 2023-09-09T00:27:15
Go Ver      : go1.17.5

Usage of ./cmd/ogps/ogps:
  -b uint
        gps serial baud rate (default 9600)
  -s string
        gps serial name (default "/dev/serial0")
  -t    testing mode
  -v    verbose mode
  -vv
        very verbose mode
```

## Ready to use Raspberry image
The easiest way for running OGPS is to download the ready to use image for raspberry >=Pi3: [ogps-rpiv04.img.zip](https://lemoidului.s3.eu-west-3.amazonaws.com/gitlab/public/releases/ogps/img/ogps-rpiv04.img.zip)  
Image contains OGN binaries, overlayfs enable, correct configuration for hardware UART and last OGPS release.  
Download zip image, unzip && write image on micro-sd card, edit OGN-receiver.conf file in fat partition with just the name of your receiver (mandatory) and wifi params if need (optional).  
Put the sd card on Rpi and boot, that's it.

## Manually Install
The first step is to [build an ogn receiver](http://wiki.glidernet.org/ogn-receiver-installation) and configure hardware UART on it.  
> **Special Note: use TTL UART on Raspberry >= Pi3**  
*Since rapsberry Pi version 3, there's two UART on Raspberry Pi board.  
One is internally connect to bluetooth module (PL011 UART) and the second (mini UART) is available through GPIO ports on the board.  
**As only the uart served by the PL011 can guarantee a good baudrate stability, you must configure your board and use the PL011 for wiring your gps module**  
Please, [follow carrefuly this documentation](doc/pi_serial.md).*   

Then, [download latest OGPS binary package](https://gitlab.com/lemoidului/ogps/-/releases).  
The install script inside package will install ogps arm 32 bits version in /usr/local/sbin/ and ogps service in /etc/systemd/system. 
```bash
# if you use ogn receiver rpi image, disable overlayfs
sudo overlayctl disable
sudo reboot
# once reboot
cd /tmp
wget <ogps release package url>
tar -xvzf ogps-<version>.tar.gz
cd ogps-<version>
sudo ./install.sh
# check if ogps is working (because some ogn process run as root you need running ogps in root as well)
sudo ogps
...
[CTRL] + C
# you can also use verbose or very verbose mode
sudo ogps -v 
# or 
sudo ogps -vv
...
[CTRL] + C
# if you use ogn receiver image, re-enabled overlayfs
sudo overlayclt enable
sudo reboot
# once reboot, check if ogps daemon is working fine
sudo journalctl -u ogps -f
[CTRL] + C
```
OGPS systemd unit is set enable during install process (active for next reboot).  
During boot time, ogps service will be start after rtlsdr-ogn service.  

## Build your own package
prerequisite:
 - git
 - go version >= 1.17.5
 - make

 Makefile will produce binaries for linux amd-64bits, linux arm-32 and linux arm-64 bits.  
 ```bash
 git clone git@gitlab.com:lemoidului/ogps.git
 cd ogps
 make build
 or
 make dist
 ```
