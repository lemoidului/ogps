OGPS = $(CURDIR)/cmd/ogps
DIST = $(CURDIR)/dist

M = $(shell printf "\033[34;1m▶\033[0m")
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null)
DATE    ?= $(shell date +%FT%T)

LDFLAGS = -w -s
LDFLAGS:=$(LDFLAGS) -X ogps/cmd.Version=$(VERSION) -X ogps/cmd.BuildDate=$(DATE)
SOURCES := $(shell find $(CURDIR) -name "*.go")

$(OGPS)/ogps: $(SOURCES)
	GOOS=linux go build -ldflags '$(LDFLAGS)' -o $@ $(OGPS)/main.go
$(OGPS)/ogps-arm: $(SOURCES)
	GOOS=linux GOARCH=arm go build -ldflags '$(LDFLAGS)' -o $@ $(OGPS)/main.go
$(OGPS)/ogps-arm64: $(SOURCES)
	GOOS=linux GOARCH=arm64 go build -ldflags '$(LDFLAGS)' -o $@ $(OGPS)/main.go

build: $(OGPS)/ogps

OBJS=$(OGPS)/ogps $(OGPS)/ogps-arm $(OGPS)/ogps-arm64 $(DIST)/ogps.service $(DIST)/install.sh
$(DIST)/ogps-$(VERSION).tar.gz: $(OBJS)
	$(eval TAR=$(notdir $@))
	@mkdir -p $(DIST)/ogps-$(VERSION)
	@cp $(OGPS)/ogps $(DIST)/ogps-$(VERSION)/
	@cp $(OGPS)/ogps-arm $(DIST)/ogps-$(VERSION)/
	@cp $(OGPS)/ogps-arm64 $(DIST)/ogps-$(VERSION)/
	@cp $(DIST)/ogps.service $(DIST)/ogps-$(VERSION)/ogps.service
	@cp $(DIST)/install.sh $(DIST)/ogps-$(VERSION)/install.sh
	$(info $(M) create archive ..)
	@(cd $(DIST) ; tar -czf $(TAR) --remove-files ogps-$(VERSION))

dist: $(DIST)/ogps-$(VERSION).tar.gz

clean: ; $(info $(M) cleaning …)
	rm -f $(OGPS)/ogps
	rm -f $(OGPS)/ogps-arm
	rm -f $(OGPS)/ogps-arm64
	rm -f $(DIST)/ogps-$(VERSION).tar.gz

version:
	@echo $(VERSION)

all: build

.PHONY: build all version clean dist
